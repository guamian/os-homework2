#include <stdio.h>
#include <assert.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define SOCK_PATH "/tmp/soc"

void chld_handler(int sig) {
    pid_t pid;
    int status;

    while ((pid=waitpid(-1, &status, 0)) != -1) {
       // handle finished child
    }
}

static void daemonize(void) {
    pid_t pid, sid;

    if ( getppid() == 1 ) return;

    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
        perror("Error when fork");
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
        perror("Error with setsid");
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
        perror("Error with directory");
    }

    // redirect fd0, fd1, fd2 to /dev/null
    freopen( "/dev/null", "r", stdin);
    freopen( "/dev/null", "w", stdout);
    freopen( "/dev/null", "w", stderr);

    int minpid=getpid();
    FILE *fp;
    fp = fopen("/tmp/minpid.txt", "w" ); // Open file for writing
    fprintf(fp, "%d", minpid);
    fclose(fp);
}

static void kommandchild(char *argument){
    switch(fork()){
        case 0:
            execlp("/bin/sh","sh","-c",argument,NULL);
            perror("Error when exec");
            exit(1);
            break;
        case -1:
            perror("Error when fork");
            exit(1);
    }
    wait(0);
}

static void brev(){
    switch(fork()){
        case 0:
            execlp("/bin/echo","echo","\nKLART!\n",NULL);
            perror("Error when exec");
            exit(1);
            break;
        case -1:
            perror("Error when fork");
            exit(1);
    }
    wait(0);
}

int changeDirectory(char *command) {
    int way = 0, sign = 0;
    char path[100], path2[100];
    strcpy(path2,command);

    if(path2[0] == 99 && path2[1] == 100 ) {
        way = 1;
        if((path2[2] == 0) || (path2[3] == 0)) {
            sign = 1;
        }
        if((path2[3] == 47) || (path2[4] == 47)) {
            sign = 3;
        }
    }

    if (sign == 1) {
        chdir("/home/me");
    }
    if (sign == 3) {
        int i;
        for(i = 0;i < 90;i++) {
            path[i] = path2[i + 3];
        }
        chdir(path);
    }
    return way;
}

int main( int argc, char *argv[] ){
    daemonize();

    int fds, fds2, len2, len, test;
    struct sockaddr_in local, remote;
    char ord[100];

    //skapa socket till process

    fds = socket(AF_INET, SOCK_STREAM, 0);
    local.sin_family = AF_INET;
    // strcpy(local.sin_path, SOCK_PATH);
    // unlink(local.sin_path);
    // len = strlen(local.sun_path) + sizeof(local.sun_family);


    local.sin_addr.s_addr = htonl(INADDR_ANY);
    local.sin_port = htons(5000);
    //dags att binda fd till socket i process

    test=bind(fds, (struct sockaddr *)&local, sizeof(struct sockaddr));
    if(test == -1) {
        perror("Error when binding");
        return 1;
    }
    test=listen(fds, 10);
    if(test == -1) {
        perror("Error when listening");
        return 1;
    }

    ///////////WAIT (TA HAND OM AVSLUTADE BARN)/////////////
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = chld_handler;
    sigaction(SIGCHLD, &sa, NULL);

    // accept and listen
    while(1) {
        printf("waiting for connect..\n");
        len2 = sizeof(remote);
        fds2 = accept(fds, (struct sockaddr *)&remote, &len2);

        pid_t pid = fork();
        if (pid == 0) {
            // receive directory info
            char path3[100]; int t2;
            assert(t2=recv(fds2, path3, 100, 0));
            if(t2 < 0) {
                kill(getpid(),SIGINT);
            }
            path3[t2] = '\0';
            chdir(path3);

    ////////  redigera stdout och stderr //////////////////
            dup2( fds2, 1 );
            dup2( fds2, 2 );

    /////// ta upp kommando an so lange //////////////////
            char path[100];
            while(1){
                int t;
                assert(t = recv(fds2, ord, 100, 0));
                if(t < 0)
                    kill(getpid(),SIGINT);   //  exit(0);
                ord[t]='\0';

                // handle command 'cd'
                int way = 0;
                if(ord[0] == 99 && ord[1] == 100) {
                    way=1;    int i;
                    for(i = 0;i < 90;i++) {
                        path[i] = ord[i + 3];
                        if(ord[i + 3] == 10) {
                            path[i]=0;
                            break;
                        }
                    }
                    chdir(path);  // i path ligger bara katalogen   cd  "kat"
                    brev();
                }
//
//                    if((path2[2]==0) || (path2[3]==0)){
//                        sign=1;
//                    }
//                    if((path2[3]==47) || (path2[4]==47)){
//                        sign=3;
//                    }
//                }
//
//                if(sign==1){
//                    strcpy(path,"/home/me");
//                    chdir(path);
//                }
//                if(sign==3){
//                    int i;
//                    for(i=0;i<90;i++){
//                        path[i]=path2[i+3];
//                    }
//                    for(i=0;i<100;i++){
//                        if(path[i]==10){
//                            path[i]=0;
//                            path[i+1]=0;
//                            path[i+2]=0;
//                            break;
//                        }
//                    }
    ////// skicka korningen av kommando och nar det ar klart
    ////// skicka brev
                if(way == 0) {
                    pid_t pid2 = fork();
                    if (pid2 == 0){
                        kommandchild(ord);
                        exit(0);
                    } else {
                        wait(0);
                        brev();
                    }
                }
            }
            close(fds2);
            kill(getpid(),SIGINT);
        }
        close(fds2);
    }
    return 0;
}
